# Proxy: Any Application

On the heels of the [`browbeat`](https://gitlab.com/MrCharles/browbeat) project, I realized that FireFox does not have or allow command-line proxy.

It's really kind-of funky, considering FireFox is usually a little ahead of Google's Chrome in terms of privacy and security &mdash; but no command-line option to launch with a proxy? A lot of things I've looked at in the source-code behind FireFox are too integrated directly with the profile manager. Why is this stupid? Because what if someone did not target a FireFox sandbox, but gained access to the profiles behind a running FireFox instance? &mdash; Not good.

In any case, how do I simplify this process of isolating FireFox's network environment?

Oh, well, there's `tsocks` and `proxychains`. No big deal, quick, dirty and easy fix, and better than relying upon application-level security. _(For example, `Chromium` provides command-line proxy, BUT for the fact that an application doesn't need to honor its self-imposed rules; it could still scan to see if other network access is available to report information back to Google, irrespective. **But for the fact that you are ruler of the local environment and kingdom.**)_

Without further comment ...

```bash
$> sudo apt-get install proxychains
$> sudo vim /etc/proxychains.conf
# Add your local http proxy.
# For example, under the Proxy List heading, I add the following:
[Proxy List]
http 127.0.0.1 5150
# save and exit.
$> proxychains firefox
# And, firefox is offline, but, I can access servers running and configured to accept localhost connections.
```

And that's it.

My `localhost:5150` designation blocks all outbound traffic for private, offline browser instances. I mostly use this for my private Jupyter instance, so that I don't open network-active tabs when I want to keep research or development local and private only. But, you can use it for other things, too.

That said, I moved to using `proxychains` for `chromium`, too. As previously noted, instead of trusting application-level security _(which the application doesn't necessarily need to honor)_.

**More specifically:** I have not had any issues using it, even with parameterized calls. All I've done is prepend `proxychains exec -params=vars` and it just works. Any application called with `proxychains ` prepended, given the configuration ... that application is OFFLINE. _(And this makes me happy.)_

Quick, stupid-simple, functional, effective.

-----

### Side Note

One other reason I like having a local offline proxy server is for testing extensions and plugins. Obviously an extension or plugin that relies on something like DNS is just going to break. But, that's the point. Let's say, you're developing something that **should work offline** &mdash; how do you know it works offline, without a lot of workaround. Simple solution: Run your own, local offline proxy that has no access to anything but `localhost` in our ever-connected world. Does whatever you're developing still work with all of the plugins and tools if the local computer goes offline? Or is there something deeply embedded in whatever plugin or tool that somehow requires network access to function, or lack of network access _(maybe, eventually)_ breaks the application? &mdash; And the reason I make this note is because I've used applications that have memory leaks on unreturned calls to the network stack _(i.e. reporting usage information; even though it should run local only)_.

Being able to simply prepend `proxychains ` to access the proxy, without need to add parameters or network-stack-aware information to an application, _which should function offline_, simplifies testing and development. It might also help disceren bugs in applications requesting network access where not otherwise stated as a necessity for proper function or operation. **A nuanced, but important security consideration; especially in our ever-connected IoT generation.**
