# Proxy: Localhost Only

### Background

At present _(August 2019)_, starting a browser _(i.e. FireFox or Chromium)_ ... you must manually take the browser offline, sans a lot of hackish stuff. More specifically, there are flags, but, they're not well documented, or subject to change, etc.

How do you solve this problem of being wholly off-line, from the start of launching a browser?

What technology has been existed forever?

**Proxy Servers**

> Q: What's the difference between one network connected machine and another?

> A: (The) Networking.

> Q: Whose networking?

> A: Subjective.

This is to say, I can manage my local machine just like an administrator can manage local or wide area networks.

-----

### Implementation

The reason I'm documenting this under "Stupid Hacks" is because it's quick and dirt-simple to provide yourself an offline proxy for any browser.

Specifically, I'm a dedicated [Debian](https://debian.org) user, and I use the `#!++` _([Crunchbang PlusPlus](https://crunchbangplusplus.org/))_ distribution as my primary desktop. No need for Gnome or KDE or any heavyweight BS. I'm perfectly fine with the bare minimum. I don't need fancy. I want functional.

> All of you people thinking in 1337-speak that you have to do screenshots of your uber-cool desktop environment are n00bs and n0vices wasting and spending too much time on crap to help make yourself feel special; compared to what, or whom? You saw it, and you wnat it, or you want to improve-upon it and one-up someone else. How much time did you spend on that crap? Just a small bit of perspective for those of you who aren't thinking; and who think that nifty presentation gets things done faster or that those of us who've been there, done that, will somehow be awed or impressed without first thinking about the time invested _(or wasted)_.

Along those same lines, I don't want something heavy-weight like Squid.

Do a little research: Simple answer: [Tinyproxy](https://tinyproxy.github.io/).

Stupid simple to use.

```bash
$> sudo apt-get install tinyproxy
...
$> sudo vim /etc/tinyproxy/tinyproxy.conf
```

EXACTLY one line needs to be modified, but three for good measure.

The one line I modified was `Bind` &mdash; rather, I copied and modified it ...

```bash
#Bind 192.168.0.1
Bind 0.0.0.0
```

What this says is that for OUTGOING connection attempts ... you're binding to 0.0.0.0 _(not even localhost=127.0.0.1)_ &mdash; you're dumping/dropping all outbound packet requests unless you have 0.0.0.0 defined somewhere like `/etc/hosts` or on a caching DNS server that your network stack references.

So, no connection need be valid, except those accessing or referencing localhost _(127.0.0.1)_, which is caught by the local network stack before seeking external resources.

The two other lines I modified ...

```bash
#Port 8080
#Port 8888
Port 5150
#Timeout 600
Timeout 1
```

In other words, I specified the proxy `Port` as 5150, from whatever the default value was, and did the same for `Timeout` &mdash; thus the `5150` designation ... `Timeout` being set to 1 second of failure. _(Because this is a local-only implementation.)_

I did not set `Listen` &mdash; I've got multiple interfaces, and I did not want to corrupt the localhost loop. If someone wants to connect to my local machine on my internal network and use the proxy server, let them try and figure out what's going on.

And, finally ...

```bash
$> sudo service tinyproxy restart
```

So, then I modified the command-line to open-up my `chromium-jupyter` instance, as such:

```bash
# From: chromium --temp-profile --user-data-dir=$TMPDIR --incognito --new-window http://localhost:xxxxx
#   To: chromium --proxy-server=localhost:5150 --temp-profile --user-data-dir=$TMPDIR --incognito --new-window http://localhost:xxxxx
```

Launch the browser instance such that my local `jupyter` server comes up right away, and, if I try and open another tab and do some online research ... I get a 403, given by Tinyproxy.

-----
-----

There you have it. Sans _(long-form)_ explanation, the steps were quick and simple.

* Install Tinyproxy
* Configure Tinyproxy
* Restart Tinyproxy
* Implement Tinyproxy

And now I don't have to take my browser instance offline manually. I just give it a dead-drop proxy server hosted locally.

-----
-----

**Notes/Todo:**

* Check IPv6 stuffs.
* ...


