# DNS over HTTPS + dnsmasq cache

Back in 2018, I read about [Cloudflare](https://cloudflare.com)'s nifty new tool: 1.1.1.1

They took an IP address that nobody wanted and turned it into something that everybody wants.

Yeah, but what's the difference between that DNS server and any other? **HTTPS**

This is to say: Establish an HTTPS connection and retreive DNS entries over that secure connection.

This is opposed to say, your ISP hi-jacking your DNS requests and running slower DNS systems that log everything all of the time.

In any other sense of man-in-the-middle attacks, say, if a private person were to do this to another: It is typically considered "criminal" activity, subject to penalty of law. And, since most communications lines run across state lines, it's usually a federal matter when someone starts screwing with networks and packets sent over network.

But this rule doesn't apply to ISP's. They can do what they want with their networks. _(In part, this makes sense; why shouldn't you be able to what you want with stuff you own, run, manage, are responsible for...)_

And then ISP's go looking for other exceptions, such as: They're not responsible for their users' content or internet activity. Sure, log it and make use of logging it. But disclaim any other responsibility.

Pretty much all of the rules are skewed in favor of ISP's, and against consumers. That's why every couple of months articles are published about their shitty customer service, etc. As if paying for a service is not enough, those who don't want responsibility for content on their networks, also want to control those networks by hi-jacking traffic.

-----

I finally got around to setting-up a local instance of DNS over HTTPS. And, it was stupid simple.

-----

I started with a caching server, [dnsmasq](https://en.wikipedia.org/wiki/Dnsmasq).

Setting that up was as simple as installing it, and modifying a single configuration line, and then restarting

```bash
$> sudo vim /etc/dnsmasq.conf
listen-address=127.0.0.1
```

So, uncomment that line and set it to the local loop _(localhost=127.0.0.1)_.

In addition, I modified my dhclient script, with the following:

```bash
$> sudo vim /etc/dhcp/dhclient.conf
prepend domain-name-servers 127.0.0.1;
```

Finally...

```bash
$> sudo service networking restart
$> sudo service dnsmasq restart
```

-----

Then I moved-on to Cloudflare's 1.1.1.1 DNS over HTTPS service.

[Pi-hole](https://pi-hole.net), based on `Dnsmasq` has a nice "[How To](https://docs.pi-hole.net/guides/dns-over-https/)" of setting-up `Dnsmasq`+`Cloudflare DNS over HTTPS`, which is in turn based upon another "[How To](https://bendews.com/posts/implement-dns-over-https/)" by Ben Dews.

Start with getting the [Cloudflare daemon](https://developers.cloudflare.com/1.1.1.1/dns-over-https/cloudflared-proxy/). DO NOT be duped into setting-up the Argo Tunnel _(requires account)_.

After installing and making sure that the `Cloudflare daemon` is running...

Follow the above "**How To**" directions.

The simplest possible steps are to setup the system service, as explained by the **Pi-hole "How To"**.

Specifically, if you're already running `Dnsmasq` then you want to configure `cloudflared proxy-dns --port=5053...` Any other port execpt 53. Then you have to tell `Dnsmasq` where to look for DNS servers. Instead of being a public address, it is going to be the local `cloudflared proxy-dns`.

The simplest directions for configuring `Dnsmasq` are in the **Ben Dews "How To"**. It is adding the configuration file `/etc/dnsmasq.d/50-cloudflared.conf`, which has a single line:

```bash
server=127.0.0.1#5053
```

The Pi-hole "How To" shows a graphical interface specific to Pi-hole, where the Ben Dews shows you how to do it at the system level.

For good measure I took an additional step...

```bash
$> sudo vim /etc/dnsmasq.conf
no-resolve
```

I uncommented the `no-resolv` line so that the `dhclient` configured ISP settings would be ignored, and `Dnsmasq` would use the `server` line in the additional configuration file.

Then, of course...

```bash
$> sudo service networking restart
$> sudo service dnsmasq restart
$> sudo service cloudflared restart
$> dig google.com
# Thank You Kindly!
```

-----

### Problems/Issues

Take note that you could have problems or issues with this setup. For example: Cloudflare's 1.1.1.1 service goes down. If you don't have a back-up, then, your DNS goes off-line and it looks like you're having internet problems or issues. Troubleshooting or working around this is going to be problematic. It's probably best NOT to uncomment the `#no-resolve` line in your `/etc/dnsmasq.conf` file. This allows `Dnsmasq` to read other entries in case one failes. You could uncomment the `strict-order` line as more DNS over HTTPS services come online and become more standardized, but I'd recommend that instead of going the route I did, that you do the `prepend` and follow `strict-order` and allow your ISP's DNS servers to remain intact. _(Alternatively, you could also edit the `/etc/dhcp/dhclient.conf` file to tell it not to retreive DNS information when requesting an IP address; the same as if you had a static IP address, etc.)_

If you're using `Network Manager` on your system, things might be a bit different, and you should do research to that effect.

-----

### Reflections

At the end of this little endeavor earlier this morning, and over the past few days of research and testing, two or three or a few thoughts occur to me.

* This really is stupid-simple to setup.
* I have a caching dns server with security.
* However, there's a great deal of deep knowledge that I already have.
    * Maybe not so simple for others to understand where to look and what to look for and understand networking stacks, services, network configuration _(local or wide)_.
* My internet access seems a bit more snappy _(faster, quicker to load pages)_.
    * I was just a little bit surprised by this.
    * Historically: In earlier years, public DNS was often faster than ISP DNS.
    * Historically: Public DNS got slow, when ISP's started hi-jacking DNS.
        * No use fighting it; I gave-up several years ago.
    * I was exepcting things to be a little slower, or about the same, needing to go through several levels.
        * Make request.
        * Local caching/proxy turns around request.
        * Request goes through/over HTTPS
        * Request comes back.
        * DNS server caches.
        * Application consumes DNS query.
    * BUT despite a few extra steps, and running some local services with minor overhead, things seem to be going faster.
        * At least until ISP's start throttling traffic speed through/to public DNS servers.
        * This information is still available for filtering in packet headers.
        * And that filtering, simply the inspection process, or routing thereof, is enough of a headache and slow-down to make it first prohibitive, but also worth the effort for pissed-off ISP systems admins.
        * No different than filtering and throttling traffic otherwise.


That's a few more than 2-or-3 _(or a few)_ thoughts. But, that's what's on my mind. No sooner than a newly minted coin is distributed, the mint changes the mold.