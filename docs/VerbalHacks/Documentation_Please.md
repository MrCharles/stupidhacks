# Documentation, please.

If you've ever encountered someone who likes to talk more than do, then this is for you:

> "Can I have that in writing, please?"

**OR**

> "I'll need that in writing."

**OR**

> "If it's not in writing..."

... just trail-off.

-----

### An Explanation

People who like talking dislike needing to document what they've said, or what they're going to say.

One of the most common pieces of advice I've been given about handling verbal instructions is as follows:

> "After speaking to someone, follow-up with a written e-mail or memo."

**OR**

> "Write it up, send it, wait a bit _(response or enough time to review or consume the contents)_, and then speak to the person about it."

The thing here is that people who only use verbal relay of directions, or instructions, or who don't like to document things have a hard time sticking to their guns; in the vein of being able to deny accountability or responsibility later. And, so, the simple answer is to require that someone who says something also document what they've said.

Of course, obviously, it is more persusaive if someone documents what is said BEFORE speaking to you about it. But sometimes things come-up in conversation. In good keeping: Follow-up and follow-through.

And, if things come-up in conversation, and the other party is inconsistent about interpretation of conversational topics at later dates, then the obvious solution is to ask for or prompt the person for something in writing.

When this fails _(i.e. whomever speaks something, verbal communication or relay, fails to follow-up and follow-through)_, you can claim the same:

> "I don't know what you're talking about? I remember something along those lines, but I don't have anything in writing to verify."

So, if asking for something in writing is out of the question or off the table, you can play the same kind of _"I don't know anything..."_ games. Except for the fact that these are "games" that people play, and those playing this type of game usually employ some type of power disparity. If you watch a person's reaction to asking for something in writing &mdash; it's usually very telling up-front what kind of game you're playing. _(For example: Someone gets angry or dislikes a simple request like this, it's too obvious there's a problem with the situation, and you shouldn't play the game.)_

-----

### Disclaimer

Some relationships are that of: One person can verbally relay information, but another must do everything in writing. Given the obvious disparity, it's a setup for failure. Don't play these games. If you're required to produce results, in-writing, such as summaries of tasks that were verbally relayed: Before performing the task, ask for the specifics or specifications in-writing, before acting on what is being asked. And, if you never receive it in writing, it was never asked or required, except by disparity, in which case someone who only relays instructions or directions verbally can be called-out on inconsistency. On the other hand, asking for something in-writing from someone so inclined and in a position of power or authority typically runs afoul of possibly being fired for failure to perform. So, just beware, you don't want a job working for someone who is irresponsible, inconsistent, lacks accountability or lacks responsibility, and won't conform to the same standards or expectations that are required of others under their authority.

Providing written instructions or directions or documentation falls categorically into: **"Lead by example."**
