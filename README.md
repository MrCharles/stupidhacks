# StupidHacks

Just a place to document things. See the various docs.

-----

### Technology: Local Security

* [Proxy: Localhost only](./docs/Proxy_Localhost_Only.md)
* [Proxy: Any Application](./docs/Proxy_Any_Application.md)
* [DNS Over HTTPS w/ Caching](./docs/DNSoHTTPS_Cache.md)
* **ToDo:** IpTables
* ...

-----

### Verbal Hacks

This section is about small, short statements you can set to set people on edge if they're misbehaving, doing something stupid, not thinking, mind wandering, lack responsibility or accountability, etc.

* [Documentation, Please.](./docs/VerbalHacks/Documentation_Please.md)
* **ToDo:** _"Can't, Won't, or Don't"_
* **ToDo:** _"I'll remember that"_
* ...

-----

### Food Hacks

* **ToDo:** Bake, Broil, Bake
* ...
